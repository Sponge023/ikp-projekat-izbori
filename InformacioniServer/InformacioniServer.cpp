#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "conio.h"

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#pragma pack(1)

#define SERVER_PORT 27018
#define BUFFER_SIZE 256 * 2

// TCP server that use non-blocking sockets
int main()
{
	SOCKET listenSocket = INVALID_SOCKET;
	SOCKET clientSocket = INVALID_SOCKET;
	int iResult;

	char clientRequests[BUFFER_SIZE];

	// WSADATA data structure that is to receive details of the Windows Sockets implementation
	WSADATA wsaData;
	// Initialize windows sockets library for this process
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		printf("WSAStartup failed with error: %d\n", WSAGetLastError());
		return 1;
	}

	// Initialize serverAddress structure used by bind
	sockaddr_in serverAddress;
	memset((char*)&serverAddress, 0, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;				// IPv4 address family
	serverAddress.sin_addr.s_addr = INADDR_ANY;		// Use all available addresses
	serverAddress.sin_port = htons(SERVER_PORT);	// Use specific port

	// Create a SOCKET for connecting to server
	listenSocket = socket(AF_INET,      // IPv4 address family
		SOCK_STREAM,  // Stream socket
		IPPROTO_TCP); // TCP protocol
	// Check if socket is successfully created
	if (listenSocket == INVALID_SOCKET)
	{
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	// Create a SOCKET for connecting to server
	clientSocket = socket(AF_INET,      // IPv4 address family
		SOCK_STREAM,  // Stream socket
		IPPROTO_TCP); // TCP protocol
	// Check if socket is successfully created
	if (clientSocket == INVALID_SOCKET)
	{
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}

		#pragma region BIND SOCKET
		// Setup the TCP listening socket - bind port number and local address to socket
		iResult = bind(listenSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress));

		// Check if socket is successfully binded to address and port from sockaddr_in structure
		if (iResult == SOCKET_ERROR)
		{
			printf("bind failed with error: %d\n", WSAGetLastError());
			closesocket(listenSocket);
			WSACleanup();
			return 1;
		}
		bool bOptVal = true;
		int bOptLen = sizeof(bool);

		iResult = setsockopt(listenSocket, SOL_SOCKET, SO_CONDITIONAL_ACCEPT, (char*)&bOptVal, bOptLen);
		if (iResult == SOCKET_ERROR) {
			printf("setsockopt for SO_CONDITIONAL_ACCEPT failed with error: %u\n", WSAGetLastError());
		}

		unsigned long  mode = 1;

		if (ioctlsocket(listenSocket, FIONBIO, &mode) != 0)
			printf("ioctlsocket failed with error.");

		// Set listenSocket in listening mode
		iResult = listen(listenSocket, SOMAXCONN);
		if (iResult == SOCKET_ERROR)
		{
			printf("listen failed with error: %d\n", WSAGetLastError());
			closesocket(listenSocket);
			WSACleanup();
			return 1;
		}

		printf("Cekanje na glasacku kutiju.\n");
		#pragma endregion

	fd_set readfds;

	// timeout for select function
	timeval timeVal;
	timeVal.tv_sec = 1;
	timeVal.tv_usec = 0;

	while (true)
	{
		FD_ZERO(&readfds);

		FD_SET(listenSocket, &readfds);
		FD_SET(clientSocket, &readfds);

		int selectResult = select(0, &readfds, NULL, NULL, &timeVal);
		if (selectResult == SOCKET_ERROR)
		{
			printf("Select failed with error: %d\n", WSAGetLastError());
			closesocket(listenSocket);
			WSACleanup();
		}
		else if (selectResult == 0) // timeout expired
		{
			if (_kbhit()) //check if some key is pressed
			{
				_getch();
				printf("Cekaju se rezultati glasanja...\n");
			}
			continue;
		}
		else if (FD_ISSET(listenSocket, &readfds))
		{
			sockaddr_in clientAddr;
			int clientAddrSize = sizeof(struct sockaddr_in);

			clientSocket = accept(listenSocket, (struct sockaddr*)&clientAddr, &clientAddrSize);
			if (clientSocket == INVALID_SOCKET)
			{
				if (WSAGetLastError() == WSAECONNRESET)
				{
					printf("accept failed, because timeout for client request has expired.\n");
				}
				else
				{
					printf("accept failed with error: %d\n", WSAGetLastError());
				}
			}
			else
			{
				if (ioctlsocket(clientSocket, FIONBIO, &mode) != 0)
				{
					printf("ioctlsocket failed with error.");
					continue;
				}
				printf("Glascka kutija je konektovana.\n");
			}
		}
		else
		{
			if (FD_ISSET(clientSocket, &readfds))
			{
				iResult = recv(clientSocket, clientRequests, BUFFER_SIZE, 0);
				if (iResult > 0)
				{
					clientRequests[iResult] = '\0';

					printf("%s", clientRequests);

					break;
				}
			}
		}
	}

	printf("\nPritisnite dugme da izadjete iz aplikacije.");
	_getch();

	//Close listen and accepted sockets
	closesocket(listenSocket);
	closesocket(clientSocket);

	// Deinitialize WSA library
	WSACleanup();

	return 0;
}