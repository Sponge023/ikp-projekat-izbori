#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "conio.h"

#include <chrono>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define SERVER_IP_ADDRESS "127.0.0.1"
#define SERVER_PORT 27016
#define BUFFER_SIZE 256

// Svaki klijent pokrece se u zasebnoj niti, koje izvrsavaju funkciju clientConnect
DWORD WINAPI clientConnect(LPVOID lpParam)
{
	// Socket used to communicate with server
	SOCKET connectSocket = INVALID_SOCKET;

	// Variable used to store function return value
	int iResult;

	// Buffer we will use to store message
	char dataBuffer[BUFFER_SIZE];

	// WSADATA data structure that is to receive details of the Windows Sockets implementation
	WSADATA wsaData;

	// Initialize windows sockets library for this process
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) 
	{
		printf("WSAStartup failed with error: %d\n", WSAGetLastError());
	}

	// Create a socket
	connectSocket = socket(
		AF_INET,		// IPv4 address family
		SOCK_STREAM,	// Stream socket
		IPPROTO_TCP);	// TCP protocol

	// Check if socket created successfully
	if (connectSocket == INVALID_SOCKET) 
	{
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
	}

	// Create and initialize address structure
	sockaddr_in serverAddress;
	serverAddress.sin_family = AF_INET;								// IPv4 protocol
	serverAddress.sin_addr.s_addr = inet_addr(SERVER_IP_ADDRESS);	// IP address of server
	serverAddress.sin_port = htons(SERVER_PORT);					// Server port

	// Connect to server specified in serverAddress and socket connectSocket
	if (connect(connectSocket, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR) {
		printf("Glasanje je zavrseno.\n");
		closesocket(connectSocket);
		WSACleanup();
	}

	while (1) {
		iResult = recv(connectSocket, dataBuffer, BUFFER_SIZE, 0);
		if (iResult > 0)	// Check if message is successfully received
		{
			dataBuffer[iResult] = '\0';

			// Log message text
			printf("%s\n", dataBuffer);
		}
		else if (iResult == 0)	// Check if shutdown command is received
		{
			printf("Shutting down.\n");
			break;
		}
		else {
			if (WSAGetLastError() == WSAEWOULDBLOCK) {
				Sleep(1000);
				printf("Waiting for response.\n");
			}
		}
		
		memset(dataBuffer, 0, BUFFER_SIZE);

		printf("Unesite opciju za koju glasate: ");
		gets_s(dataBuffer, BUFFER_SIZE);
		strcat(dataBuffer, " <VREMENSKA OZNAKA>: ");

		// current date/time based on current system
		time_t now = time(0);
		// convert now to string form
		char* dt = ctime(&now);
		strcat(dataBuffer, dt);

		// Send message to server using connected socket
		iResult = send(connectSocket, dataBuffer, (int)strlen(dataBuffer), 0);
		// Check result of send function
		if (iResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(connectSocket);
			WSACleanup();
		}

		memset(dataBuffer, 0, BUFFER_SIZE);
		iResult = recv(connectSocket, dataBuffer, BUFFER_SIZE, 0);
		if (iResult > 0)	// Check if message is successfully received
		{
			dataBuffer[iResult] = '\0';

			// Log message text
			printf("%s\n", dataBuffer);
		}
		else if (iResult == 0)	// Check if shutdown command is received
		{
			printf("Shutting down.\n");
			break;
		}
		else {
			if (WSAGetLastError() == WSAEWOULDBLOCK) {
				Sleep(1000);
				printf("Waiting for response.\n");
			}
		}

		break;
	}

	// Shutdown the connection since we're done
	iResult = shutdown(connectSocket, SD_BOTH);
	// Check if connection is succesfully shut down.
	if (iResult == SOCKET_ERROR) {
		printf("Shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(connectSocket);
		WSACleanup();
	}

	printf("\nPritisnite dugme da izadjete iz aplikacije.");
	_getch();

	// Close connected socket
	closesocket(connectSocket);

	// Deinitialize WSA library
	WSACleanup();

	return 0;
}

int main(void)
{
	DWORD clientConnectID;
	HANDLE hClientConnect;
	hClientConnect = CreateThread(NULL, 0, &clientConnect, NULL, 0, &clientConnectID);

	// Blokiraj main dok klijent ne odgovori, ili ga ugasi nakon 1 minut neaktivnosti.
	WaitForSingleObject(hClientConnect, 60000);

	CloseHandle(hClientConnect);
	return 0;
}