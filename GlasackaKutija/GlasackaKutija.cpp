#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "conio.h"
#include "List.h"
#include <chrono>
#include <iostream>

#pragma pack(1)

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define CLIENTS_PORT 27016
#define WORKERS_PORT 27017
#define INFORMATIONS_PORT 27018
#define BUFFER_SIZE 256
#define SERVER_IP_ADDRESS "127.0.0.1"

#define MAX_CLIENTS 100

typedef struct results
{
	int brojaci[4];
} REZULTATI;

using namespace std;

int main()
{
		#pragma region DECLARATIONS
	// Declaring main list
	List listaGlasova;
	REZULTATI *results;

	// current date/time based on current system
	time_t now;
	struct tm nowLocal;
	struct tm endLocal;
	now = time(NULL);
	nowLocal = *localtime(&now);
	
	cout << "DATUM: " << nowLocal.tm_mday << " " << nowLocal.tm_mon + 1 << " " << nowLocal.tm_year + 1900 << endl;
	cout << "GLASACKA KUTIJA OTVORENA U: " << nowLocal.tm_hour << ":" << nowLocal.tm_min << ":" << nowLocal.tm_sec << endl;

	endLocal = nowLocal;
	// GLASANJE TRAJE 1 MINUT, TESTNI SLUCAJ, PROMENITI DA SE PRODUZI
	endLocal.tm_sec += 30;

	cout << "GLASACKA KUTIJA SE ZATVARA U: " << endLocal.tm_hour << ":" << endLocal.tm_min << ":" << endLocal.tm_sec << endl;

	time_t t1;
	time_t t2 = mktime(&endLocal);
	double diffSec = 0;

	// Socket used for listening for new clients and workers
	SOCKET listenClients = INVALID_SOCKET;

	// Sockets used for communication with clients and workers
	SOCKET clientSockets[MAX_CLIENTS];
	SOCKET workerSocket = INVALID_SOCKET;
	SOCKET informationsSocket = INVALID_SOCKET;
	
	// Initialize sockets
	memset(clientSockets, 0, MAX_CLIENTS * sizeof(SOCKET));

	// Index of arrays of clients and workers
	short lastIndexClients = 0;

	// Variable used to store function return value
	int iResult;

	// Buffer used for storing incoming data from (clients)
	char clientRequests[BUFFER_SIZE];
	char workerResponses[BUFFER_SIZE];
	char acceptMessageForClient[BUFFER_SIZE] = "Glasacki list evidentiran, obrada pocinje nakon zavrsetka glasanja.";
	char listOfChoices[BUFFER_SIZE * 2] = "GLASACKI LIST:\n1. Drasko Stanivukovic - PDP\n2. Sasa Radulovic - DJB\n3. Bosko Obradovic - DVERI\n4. Milos Jovanovic - DSS";
	char resultsForInformations[BUFFER_SIZE * 2];

	// WSADATA data structure that is to receive details of the Windows Sockets implementation
	WSADATA wsaData;
	// Initialize windows sockets library for this process
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) 
	{
		printf("WSAStartup failed with error: %d\n", WSAGetLastError());
		return 1;
	}
		#pragma endregion

		#pragma region ADDRESS STRUCTURES
	// Initialize serverAddress structure used by bind for clients
	sockaddr_in serverAddressforClients;
	memset((char*)&serverAddressforClients, 0, sizeof(serverAddressforClients));
	serverAddressforClients.sin_family = AF_INET;				// IPv4 address family
	serverAddressforClients.sin_addr.s_addr = INADDR_ANY;		// Use all available addresses
	serverAddressforClients.sin_port = htons(CLIENTS_PORT);		// Use specific port
		#pragma endregion

		#pragma region LISTEN SOCKETS
	// Create a SOCKET for clients to connect to server
	listenClients = socket(
		AF_INET,      // IPv4 address family
		SOCK_STREAM,  // Stream socket
		IPPROTO_TCP); // TCP protocol
	// Check if socket is successfully created
	if (listenClients == INVALID_SOCKET) 
	{
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
		#pragma endregion

		#pragma region BIND CLIENTS
	// Setup the TCP listening socket - bind port number and local address to socket
	iResult = bind(listenClients, (struct sockaddr*)&serverAddressforClients, sizeof(serverAddressforClients));
	// Check if socket is successfully binded to address and port from sockaddr_in structure
	if (iResult == SOCKET_ERROR)
	{
		printf("bind failed with error: %d\n", WSAGetLastError());
		closesocket(listenClients);
		WSACleanup();
		return 1;
	}

	bool bOptVal = true;
	int bOptLen = sizeof(bool);

	iResult = setsockopt(listenClients, SOL_SOCKET, SO_CONDITIONAL_ACCEPT, (char*)&bOptVal, bOptLen);
	if (iResult == SOCKET_ERROR) 
	{
		printf("setsockopt for SO_CONDITIONAL_ACCEPT failed with error: %u\n", WSAGetLastError());
	}

	unsigned long mode = 1;

	if (ioctlsocket(listenClients, FIONBIO, &mode) != 0)
	{
		printf("ioctlsocket failed with error.");
	}

	// Set listenSocket in listening mode
	iResult = listen(listenClients, SOMAXCONN);
	if (iResult == SOCKET_ERROR)
	{
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(listenClients);
		WSACleanup();
		return 1;
	}
	printf("\nCekaju se klijenti...\n");
		#pragma endregion

	// Set of socket descriptors
	fd_set readfds;
	fd_set readfds2;

	// Timeout for select function
	timeval timeVal;
	timeVal.tv_sec = 1;
	timeVal.tv_usec = 0;

	while (true)
	{
		#pragma region INITIALIZE SOCKET SETS
		// Initialize socket set
		FD_ZERO(&readfds);

		// Add server's listen socket and clients' sockets to set
		if (lastIndexClients != MAX_CLIENTS)
		{
			FD_SET(listenClients, &readfds);
		}

		for (int i = 0; i < lastIndexClients; i++)
		{
			FD_SET(clientSockets[i], &readfds);
		}
		#pragma endregion

		// Wait for events on set
		int selectResult = select(0, &readfds, NULL, NULL, &timeVal);
		if (selectResult == SOCKET_ERROR)
		{
			printf("Select failed with error: %d\n", WSAGetLastError());
			closesocket(listenClients);
			WSACleanup();
			return 1;
		}
		else if (selectResult == 0)
		{
			now = time(NULL);
			nowLocal = *localtime(&now);
			cout << "TRENUTNO VREME: " << nowLocal.tm_hour << ":" << nowLocal.tm_min << ":" << nowLocal.tm_sec << endl;
			t1 = mktime(&nowLocal);
			diffSec = difftime(t1, t2);
			if (!diffSec)
			{
				break;
			}
			continue;
		}
		else if (FD_ISSET(listenClients, &readfds))
		{
			#pragma region EVENT ON CLIENT SET
			// Struct for information about connected client
			sockaddr_in clientAddress;
			int clientAddressSize = sizeof(struct sockaddr_in);

			if (lastIndexClients <= 2)
			{
				// New connection request is received. Add new socket in array on first free position.
				clientSockets[lastIndexClients] = accept(listenClients, (struct sockaddr*)&clientAddress, &clientAddressSize);

				if (clientSockets[lastIndexClients] == INVALID_SOCKET)
				{
					if (WSAGetLastError() == WSAECONNRESET)
					{
						printf("accept failed, because timeout for client request has expired.\n");
					}
					else
					{
						printf("accept failed with error: %d\n", WSAGetLastError());
					}
				}
				else
				{
					if (ioctlsocket(clientSockets[lastIndexClients], FIONBIO, &mode) != 0)
					{
						printf("ioctlsocket failed with error.");
						continue;
					}
					lastIndexClients++;
					printf("Novi klijent se konektovao sa adrese: %s:%d\n", inet_ntoa(clientAddress.sin_addr), ntohs(clientAddress.sin_port));

					iResult = send(clientSockets[lastIndexClients - 1], listOfChoices, (int)strlen(listOfChoices), 0);
					// Check result of send function
					if (iResult == SOCKET_ERROR) {
						printf("send failed with error: %d\n", WSAGetLastError());
						closesocket(clientSockets[lastIndexClients]);
						WSACleanup();
						return 1;
					}
				}
			}
			else
			{
				#pragma region CHECK FOR CLIENT MESSAGES
				// Check if new message is received from connected clients
				for (int i = 0; i < lastIndexClients; i++)
				{
					// Check if new message is received from client on position "i"
					if (FD_ISSET(clientSockets[i], &readfds))
					{
						iResult = recv(clientSockets[i], clientRequests, BUFFER_SIZE, 0);
						if (iResult > 0)
						{
							clientRequests[iResult] = '\0';

							switch (clientRequests[0])
							{
							case '1':
								printf("\nKlijent (%d), bira: %c. Drasko Stanivukovic - PDP\n", i + 1, clientRequests[0]);
								listaGlasova.Dodaj(clientRequests[0]);
								printf("Glasacki list uspesno evidentiran.\n");
								break;
							case '2':
								printf("\nKlijent (%d), bira: %c. Sasa Radulovic - DJB\n", i + 1, clientRequests[0]);
								listaGlasova.Dodaj(clientRequests[0]);
								printf("Glasacki list uspesno evidentiran.\n");
								break;
							case '3':
								printf("\nKlijent (%d), bira: %c. Bosko Obradovic - DVERI\n", i + 1, clientRequests[0]);
								listaGlasova.Dodaj(clientRequests[0]);
								printf("Glasacki list uspesno evidentiran.\n");
								break;
							case '4':
								printf("\nKlijent (%d), bira: %c. Milos Jovanovic - DSS\n", i + 1, clientRequests[0]);
								listaGlasova.Dodaj(clientRequests[0]);
								printf("Glasacki list uspesno evidentiran.\n");
								break;
							default:
								printf("Glasacki listic nije vazeci, odbacuje se.\n");
								break;
							}

							// Send message to server using connected socket
							iResult = send(clientSockets[i], acceptMessageForClient, (int)strlen(acceptMessageForClient), 0);
							// Check result of send function
							if (iResult == SOCKET_ERROR)
							{
								printf("Error sending message to client.\n");
							}
						}
						else
						{
							printf("Konekcija sa klijentom (%d) zatvorena.\n", i + 1);
							closesocket(clientSockets[i]);

							// Sredi niz, oslobodi mesto za novog klijenta.
							for (int j = i; j < lastIndexClients - 1; j++)
							{
								clientSockets[j] = clientSockets[j + 1];
							}
							clientSockets[lastIndexClients - 1] = 0;

							lastIndexClients--;
						}
					}
				}
				#pragma endregion
			}
			#pragma endregion
		}
		else
		{
			#pragma region CHECK FOR CLIENT MESSAGES
			// Check if new message is received from connected clients
			for (int i = 0; i < lastIndexClients; i++)
			{
				// Check if new message is received from client on position "i"
				if (FD_ISSET(clientSockets[i], &readfds))
				{
					iResult = recv(clientSockets[i], clientRequests, BUFFER_SIZE, 0);
					if (iResult > 0)
					{
						clientRequests[iResult] = '\0';

						switch (clientRequests[0])
						{
						case '1':
							printf("\nKlijent (%d), bira: %c. Drasko Stanivukovic - PDP\n", i + 1, clientRequests[0]);
							listaGlasova.Dodaj(clientRequests[0]);
							printf("Glasacki list uspesno evidentiran.\n");
							break;
						case '2':
							printf("\nKlijent (%d), bira: %c. Sasa Radulovic - DJB\n", i + 1, clientRequests[0]);
							listaGlasova.Dodaj(clientRequests[0]);
							printf("Glasacki list uspesno evidentiran.\n");
							break;
						case '3':
							printf("\nKlijent (%d), bira: %c. Bosko Obradovic - DVERI\n", i + 1, clientRequests[0]);
							listaGlasova.Dodaj(clientRequests[0]);
							printf("Glasacki list uspesno evidentiran.\n");
							break;
						case '4':
							printf("\nKlijent (%d), bira: %c. Milos Jovanovic - DSS\n", i + 1, clientRequests[0]);
							listaGlasova.Dodaj(clientRequests[0]);
							printf("Glasacki list uspesno evidentiran.\n");
							break;
						default:
							printf("Glasacki listic nije vazeci, odbacuje se.\n");
							break;
						}

						// Send message to server using connected socket
						iResult = send(clientSockets[i], acceptMessageForClient, (int)strlen(acceptMessageForClient), 0);
						// Check result of send function
						if (iResult == SOCKET_ERROR)
						{
							printf("Error sending message to client.\n");
						}
					}
					else
					{
						printf("Konekcija sa klijentom (%d) zatvorena.\n", i + 1);
						closesocket(clientSockets[i]);

						// Sredi niz, oslobodi mesto za novog klijenta.
						for (int j = i; j < lastIndexClients - 1; j++)
						{
							clientSockets[j] = clientSockets[j + 1];
						}
						clientSockets[lastIndexClients - 1] = 0;

						lastIndexClients--;
					}
				}
			}
					#pragma endregion
		}
	}

	printf("\nGLASACKA KUTIJA ZATVORENA.\n");
	
	// Create a socket
	workerSocket = socket(
		AF_INET,		// IPv4 address family
		SOCK_STREAM,	// Stream socket
		IPPROTO_TCP);	// TCP protocol

	// Check if socket created successfully
	if (workerSocket == INVALID_SOCKET)
	{
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
	}

	// Create and initialize address structure
	sockaddr_in serverAddress;
	serverAddress.sin_family = AF_INET;								// IPv4 protocol
	serverAddress.sin_addr.s_addr = inet_addr(SERVER_IP_ADDRESS);	// IP address of server
	serverAddress.sin_port = htons(WORKERS_PORT);					// Server port

	// Connect to server specified in serverAddress and socket connectSocket
	if (connect(workerSocket, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR) {
		printf("Unable to connect to server.\n");
		closesocket(workerSocket);
		WSACleanup();
	}

	const char* listStr = listaGlasova.ListToString();
	//printf("\n%s\n", listStr);
	iResult = send(workerSocket, listStr, (int)strlen(listStr), 0);
	// Check result of send function
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(workerSocket);
		WSACleanup();
	}

	iResult = recv(workerSocket, clientRequests, BUFFER_SIZE, 0);
	if (iResult > 0)
	{
		clientRequests[iResult] = '\0';

		results = (REZULTATI *)clientRequests;
		double percents[4];
		//printf("%d", strlen(listStr));
		percents[0] = (double)((double)results->brojaci[0] / (double)strlen(listStr)) * 100;
		percents[1] = (double)((double)results->brojaci[1] / (double)strlen(listStr)) * 100;
		percents[2] = (double)((double)results->brojaci[2] / (double)strlen(listStr)) * 100;
		percents[3] = (double)((double)results->brojaci[3] / (double)strlen(listStr)) * 100;
		sprintf(resultsForInformations, "\nREZULTATI GLASANJA:\n1. Drasko Stanivukovic - PDP: %d (%.2f %%)\n2. Sasa Radulovic - DJB: %d (%.2f %%)\n3. Bosko Obradovic - DVERI: %d (%.2f %%)\n4. Milos Jovanovic - DSS: %d (%.2f %%)\n\0", results->brojaci[0], percents[0], results->brojaci[1], percents[1], results->brojaci[2], percents[2], results->brojaci[3], percents[3]);
	}

	//printf("%s", resultsForInformations);

	// Create a socket
	informationsSocket = socket(
		AF_INET,		// IPv4 address family
		SOCK_STREAM,	// Stream socket
		IPPROTO_TCP);	// TCP protocol
	// Check if socket created successfully
	if (informationsSocket == INVALID_SOCKET)
	{
		printf("socket failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
	}

	// Create and initialize address structure
	sockaddr_in informationAddress;
	informationAddress.sin_family = AF_INET;									// IPv4 protocol
	informationAddress.sin_addr.s_addr = inet_addr(SERVER_IP_ADDRESS);		// IP address of server
	informationAddress.sin_port = htons(INFORMATIONS_PORT);					// Server port

	// Connect to server specified in serverAddress and socket connectSocket
	if (connect(informationsSocket, (SOCKADDR*)&informationAddress, sizeof(informationAddress)) == SOCKET_ERROR) {
		printf("Unable to connect to server.\n");
		closesocket(informationsSocket);
		WSACleanup();
	}

	iResult = send(informationsSocket, resultsForInformations, (int)strlen(resultsForInformations), 0);
	// Check result of send function
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(informationsSocket);
		WSACleanup();
	}

	printf("\nPritisnite dugme da izadjete iz aplikacije.");
	_getch();

	//Close listen and accepted sockets
	closesocket(listenClients);
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		closesocket(clientSockets[i]);
	}
	closesocket(workerSocket);
	closesocket(informationsSocket);

	// Deinitialize WSA library
	WSACleanup();

	return 0;
}