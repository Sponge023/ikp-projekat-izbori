#include "List.h"

#include <iostream>


struct node {
    char podatak;
    struct node* next;
};

struct node* tail;  // Pokazivac na kraj
struct node* head;  // Pokazivac na pocetak
struct node* current; // Pokazivac na trenutni

void List::Dodaj(char glas)
{
    struct node* novi = (struct node*)malloc(sizeof(struct node));

    if (novi == NULL)
    {
        printf("Unable to allocate memory.");
    }
    else
    {
        novi->podatak = glas; // Link data part
        novi->next = head; // Link address part

        head = novi;          // Make newNode as first node

        //printf("DATA INSERTED SUCCESSFULLY\n");
    }
}

void List::PrikaziSve()
{
    current = head;
    printf("\n");
    int i = 0;
    while (current != NULL) {
        printf("%d", ++i);
        printf("\t%c\n", current->podatak);
        current = current->next;
    }
    printf("END OF LIST\n");
}

const char* List::ListToString()
{
    static char retVal[100];
    int brojac = 0;
    current = head;
    while (current != NULL) {
        retVal[brojac] = current->podatak;
        current = current->next;
        brojac++;
    }
    return retVal;
}