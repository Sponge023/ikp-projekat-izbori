#include <stdio.h>

class timeClass
{
    // Private variables
    int tm_hour;
    int tm_year;
    int tm_min;
    int tm_mon;
    int tm_sec;
    int tm_mday;

public:
    // Getters
    int getHour() { return tm_hour; }
    int getYear() { return tm_year; }
    int getMin() { return tm_min; }
    int getMon() { return tm_mon; }
    int getSec() { return tm_sec; }
    int getMDay() { return tm_mday; }

    // Setters
    void setHour(int hour) { tm_hour = hour; }
    void setMin(int min) { tm_min = min; }
    void setSec(int sec) { tm_sec = sec; }
    void setMDay(int mday) { tm_mday = mday; }
    void setMon(int mon) { tm_mon = mon; }
    void setYear(int year) { tm_year = year; }

	bool compareDates(timeClass t1, timeClass t2)
	{
		if (t1.getYear() > t2.getYear())
		{
			return true;
		}
		else if (t1.getYear() == t2.getYear())
		{
			if (t1.getMon() > t2.getMon())
			{
				return true;
			}
			else if (t1.getMon() == t2.getMon())
			{
				if (t1.getMDay() > t2.getMDay())
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	void printDate(timeClass t)
	{
		printf("%d.%d.%d. ; %d:%d:%d", t.getMDay(), t.getMon(), t.getYear(), t.getHour(), t.getMin(), t.getSec());
	}
};